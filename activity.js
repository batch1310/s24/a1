// users with 's' in their firstname or 'd' on their lastname
// show only firstname and lastname, hide the _id field
db.users.find(
	{	
		$or:[
			{firstName: { $regex: 'S', $options: 'i'}},
			{lastName: { $regex: 'D', $options: 'i'}}
		]
	},
	{
		_id: 0, firstName: 1, lastName: 1
	}
);

// find users from HR Department and their age is greater than or equal to 70
db.users.find(
	{
		$and: [
			{ department: "HR"},
			{ age: { $gte: 70}}
		]
	}
);

// find users with the letter 'e' in firstname and age less than or equal to 30
db.users.find(
	{
		$and: [
			{firstName: { $regex: 'e', $options: 'i'}},
			{ age: { $lte: 30}}
		]
	}
);